# ELIXIR HTTP PROXY

Using https://github.com/KazuCocoa/http_proxy

## Elixir Shell

```
sudo docker-compose run --rm shell
```


### Inside the Elixir Shell

```bash
cd app/http_proxy
```

```
mix deps.get && mix compile
```


### Host Shell

Open a new terminal tab and...

```bash
sudo docker-compose up http-dev
```


### On Browser

#### Country.io

http://country.io/continent.json

Working proxy: http://localhost:8080/continent.json

#### Rescountries.eu

https://restcountries.eu/#api-endpoints-all

Not working: http://localhost:8081/rest/v2/all


## Http Proxy Config

To change urls edit `app/http_proxy/config/dev.exs` and restart the server with by doing `ctrl + c` in the terminaql window where it is running and then:

```bash
sudo docker-compose up http-dev
```

