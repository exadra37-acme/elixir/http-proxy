use Mix.Config

config :http_proxy,
  proxies: [
             %{port: 8080,
               to:   "http://country.io"},
             %{port: 8081,
               to:   "https://restcountries.eu"},
             %{port: 8082,
               to:   "https://example.com"},
             %{port: 8083,
               to:   "https://example.org"}
            ],
  timeout: 20_000, # ms
  record: false,
  play: false,
  export_path: "test/example",
  play_path: "test/data"

config :logger, :console,
  format: "\n$date $time [$level] $metadata$message",
  metadata: [:user_id],
  level: :debug
